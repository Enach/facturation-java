Facturation
-----------


Context du projet
------------------

Projet facturation des ligues de la M2L mené par Tanguy Rentet et Nicolas Hochart
Le projet suit le context M2L proposé par l'Education National
L'application permet les fonctionnalitées listées ci-dessous


Fonctionnalité
--------------

* Création d'un ligue et ajout de son trésorier
* Liste des ligues et de leur trésiorier
* Création de prestations (à facturer)
* Liste des prestations (à facturer)
* Création de facture associé à une ligue
* Liste des facutre associées à une ligue


Librairies utilisées
--------------------

* iTextPDF (v5.5.9) =====> Génération de PDF
* mail (v1.4.1) =====> Envoie de mail via l'API Google
* mysql-connector-java (v5.0.5) =====> Connection à la base de données


Base de données
---------------

Le script de création de la structure de la base de données est disponible dans le fichier "facturation.sql".
