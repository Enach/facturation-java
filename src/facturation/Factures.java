package facturation;

import gestion.Facture;
import gestion.GoogleMail;
import gestion.Ligue;
import gestion.Prestation;

import javax.mail.MessagingException;
import java.sql.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class Factures {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://51.254.220.183/facturation";

    // Database credentials
    private static final String USER = "facturation";
    private static final String PASS = "2Jhw7tNP3Xn4xVhz";

    public List<Facture> listeFactures() {
        List<Facture> factures;
        factures = new List<Facture>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Facture> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Facture facture) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Facture> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Facture> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public boolean equals(Object o) {
                return false;
            }

            @Override
            public int hashCode() {
                return 0;
            }

            @Override
            public Facture get(int index) {
                return null;
            }

            @Override
            public Facture set(int index, Facture element) {
                return null;
            }

            @Override
            public void add(int index, Facture element) {

            }

            @Override
            public Facture remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Facture> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Facture> listIterator(int index) {
                return null;
            }

            @Override
            public List<Facture> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
        Connection conn = null;
        Statement stmt = null;
        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            // STEP 3: Open a connection
            System.out.println("Connexion à la base de donnée ...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: Execute a query
            System.out.println("Création de la requête ...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM Factures";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int numero = rs.getInt("numero");
                int client = rs.getInt("client");
                Date date = rs.getDate("date");
                Ligue ligue = this.getLigueFromFacture(numero);
                int price = this.getFacturePrice(numero);

                Facture facture = new Facture(numero, client, date, ligue, price);
                factures.add(facture);

            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException ignored) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }// end finally try
        }// end try


        return factures;
    }

    public Ligue getLigueFromFacture(int numero) {
        Connection conn;
        int numeroLigue = 0;
        String nomLigue = "error";
        String tresorier = "error";
        String email_tresorier = "error";
        boolean facture_mail = false;
        String sport = "error";

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            String factureLigue;
            factureLigue = "SELECT * FROM ligue_factures WHERE facture_id=" + numero;
            ResultSet rs2 = stmt.executeQuery(factureLigue);
            String ligueReq;
            int ligueId = rs2.getInt("ligue_id");
            ligueReq = "SELECT * FROM ligues WHERE id=" + ligueId;
            ResultSet rs3 = stmt.executeQuery(ligueReq);

            numeroLigue = rs3.getInt("numero");
            nomLigue = rs3.getString("nom");
            tresorier = rs3.getString("tresorier");
            email_tresorier = rs3.getString("email_tresorier");
            facture_mail = rs3.getBoolean("facture_mail");
            sport = rs3.getString("sport");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Ligue(numeroLigue, nomLigue, tresorier, email_tresorier, facture_mail, sport);
    }

    public int getFacturePrice(int numero) {
        Connection conn;
        int total = 0;
        int prestationPrice = 0;

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            String factureLigue;
            factureLigue = "SELECT * FROM facture_prestations WHERE facture_id=" + numero;
            ResultSet rs = stmt.executeQuery(factureLigue);
            while (rs.next()) {
                // Retrieve by column name
                int prestationId = rs.getInt("prestation_id");
                prestationPrice = this.getPricePrestation(prestationId);
                total += prestationPrice;
                prestationPrice = 0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }

    public int getPricePrestation(int id) {
        Connection conn;
        int prix = 0;

        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            String factureLigue;
            factureLigue = "SELECT * FROM prestation WHERE id=" + id;
            ResultSet rs = stmt.executeQuery(factureLigue);
            prix = rs.getInt("prix");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prix;
    }

    public void addFacture(int codeClient, List<Prestation> presations, Ligue ligue) throws SQLException {
        int total = 0;
        int i = 0;
        for (i = 0; i <= presations.size(); i++) {
            total += presations.get(i).getPrix();
        }
        Facture facture = new Facture(0, codeClient, new java.util.Date(), ligue, total);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();
        String newFacture;
        newFacture = "INSERT INTO factures SET('client', 'date') VALUES(" + facture.getClient() + "," + facture.getDate() + ")";
        stmt.executeQuery(newFacture);

        String factureLigue;
        factureLigue = "INSERT INTO ligue_factures SET('ligue_id', 'facture_id') VALUES(" + ligue.getId() + "," + facture.getNumero() + ")";
        stmt.executeQuery(factureLigue);

        String prestatation;
        for (i = 0; i <= presations.size(); i++) {
            Prestation prestationObject = presations.get(i);
            prestatation = "INSERT INTO facture_prestation SET('facture_id', 'prestation_id') VALUES(" + facture.getNumero() + "," + prestationObject.getId() + ")";
            stmt.executeQuery(prestatation);
        }
        if(ligue.getFactureMail()){
            try {
                GoogleMail.Send(ligue.getEmailTresorier(), null, "Nouvelle facture", "La ligue "+ligue.getNom()+" a reçu une nouvelle facture de "+facture.getPrix()+" €");
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }
}