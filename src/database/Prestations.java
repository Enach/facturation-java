package database;

import gestion.Ligue;
import gestion.Prestation;

import java.sql.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Prestations {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://51.254.220.183/facturation";

    // Database credentials
    private static final String USER = "facturation";
    private static final String PASS = "2Jhw7tNP3Xn4xVhz";

    public List<Prestation> listePrestations() {
        List<Prestation> prestations;
        prestations = new List<Prestation>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Prestation> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Prestation prestation) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Prestation> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Prestation> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public boolean equals(Object o) {
                return false;
            }

            @Override
            public int hashCode() {
                return 0;
            }

            @Override
            public Prestation get(int index) {
                return null;
            }

            @Override
            public Prestation set(int index, Prestation element) {
                return null;
            }

            @Override
            public void add(int index, Prestation element) {

            }

            @Override
            public Prestation remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Prestation> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Prestation> listIterator(int index) {
                return null;
            }

            @Override
            public List<Prestation> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
        Connection conn = null;
        Statement stmt = null;
        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            // STEP 3: Open a connection
            System.out.println("Connexion à la base de donnée ...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: Execute a query
            System.out.println("Création de la requête ...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM prestations";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int reference = rs.getInt("reference");
                String description = rs.getString("description");
                int prix = rs.getInt("prix");

                Prestation prestation = new Prestation(reference, description, prix);
                prestations.add(prestation);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException ignored) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }// end finally try
        }// end try


        return prestations;
    }

    public void addPrestation(int reference, String description, int prix) throws SQLException {
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();
        String newPrestation;
        newPrestation = "INSERT INTO prestations SET('reference', 'description', 'prix') VALUES(" + reference + "," + description + "," + prix + ")";
        stmt.executeQuery(newPrestation);
    }
}