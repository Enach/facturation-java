package database;

import gestion.Ligue;

import java.sql.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Ligues {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://51.254.220.183/facturation";

    // Database credentials
    private static final String USER = "facturation";
    private static final String PASS = "2Jhw7tNP3Xn4xVhz";

    public List<Ligue> listeLigue() {
        List<Ligue> ligues;
        ligues = new List<Ligue>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Ligue> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Ligue ligue) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Ligue> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Ligue> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public boolean equals(Object o) {
                return false;
            }

            @Override
            public int hashCode() {
                return 0;
            }

            @Override
            public Ligue get(int index) {
                return null;
            }

            @Override
            public Ligue set(int index, Ligue element) {
                return null;
            }

            @Override
            public void add(int index, Ligue element) {

            }

            @Override
            public Ligue remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Ligue> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Ligue> listIterator(int index) {
                return null;
            }

            @Override
            public List<Ligue> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
        Connection conn = null;
        Statement stmt = null;
        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            // STEP 3: Open a connection
            System.out.println("Connexion à la base de donnée ...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: Execute a query
            System.out.println("Création de la requête ...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM ligues";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int numero = rs.getInt("numero");
                String nom = rs.getString("nom");
                String sport = rs.getString("sport");
                String tresorier = rs.getString("tresorier");
                String email = rs.getString("email_tresorier");
                boolean facture_mail = rs.getBoolean("facture_mail");
                System.out.println(numero +" "+ nom+" "+ tresorier+" "+ email+" "+ facture_mail+" "+ sport);
                Ligue ligue = new Ligue(numero, nom, tresorier, email, facture_mail, sport);
                ligues.add(ligue);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException ignored) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }// end finally try
        }// end try


        return ligues;
    }

    public void addLigue(int numero, String nom, String tresorier, String email, boolean facture_mail, String sport) throws SQLException {
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();
        String newLigue;
        newLigue = "INSERT INTO ligues SET('numero','nom', 'tresorier', 'email_tresorier', 'facture_mail','sport') VALUES(" + numero + "," + nom + "," + tresorier + "," + email + "," + facture_mail + "," + sport + ")";
        stmt.executeQuery(newLigue);
    }
}