package database;

import java.io.FileOutputStream;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import gestion.Facture;


public class FirstPdf {
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);

    public static void main(String[] args, Facture facture) {
        try {
            Document document = new Document();
            String FILE = "c:/temp/facture.pdf";
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addContent(document, facture);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // iText allows to add metadata to the PDF which can be viewed in your Adobe
    // Reader
    // under File -> Properties
    private static void addMetaData(Document document) {
        document.addTitle("Nouvelle Facture");
        document.addSubject("");
        document.addKeywords("Facture");
        document.addAuthor("M2L");
        document.addCreator("M2L");
    }

    private static void addContent(Document document, Facture facture) throws DocumentException {
        Anchor anchor = new Anchor("Facture #" + facture.getNumero(), catFont);
        anchor.setName("Facture #" + facture.getNumero());

        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 1);

        Paragraph subPara = new Paragraph("Subcategory 1", subFont);
        Section subCatPart = catPart.addSection(subPara);

        // add a table
        createTable(subCatPart, facture);

        // now add all this to the document
        document.add(catPart);
    }

    private static void createTable(Section subCatPart, Facture facture)
            throws BadElementException {
        PdfPTable table = new PdfPTable(facture.getPrestations().size() + 1);

        PdfPCell c1 = new PdfPCell(new Phrase("Reference"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Description"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Prix"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);

        for (int i = 0; i <= facture.getPrestations().size(); i++) {
            table.addCell("" + facture.getPrestations().get(i).getReference());
            table.addCell("" + facture.getPrestations().get(i).getDescription());
            table.addCell("" + facture.getPrestations().get(i).getPrix());
        }

        subCatPart.add(table);

    }
}