package gestion;

/**
 * Created by enach-laptop on 24/05/2016.
 */
public class Ligue {
    private int id;
    private int numero;
    private String nom;
    private String tresorier;
    private String emailTresorier;
    private Boolean factureMail;
    private String sport;

    public Ligue(int numero, String nom, String tresorier, String emailTresorier, Boolean factureMail, String sport) {
        this.numero = numero;
        this.nom = nom;
        this.tresorier = tresorier;
        this.emailTresorier = emailTresorier;
        this.factureMail = factureMail;
        this.sport = sport;
    }

    public int getId() {
        return id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTresorier() {
        return tresorier;
    }

    public void setTresorier(String tresorier) {
        this.tresorier = tresorier;
    }

    public String getEmailTresorier() {
        return emailTresorier;
    }

    public void setEmailTresorier(String emailTresorier) {
        this.emailTresorier = emailTresorier;
    }

    public Boolean getFactureMail() {
        return factureMail;
    }

    public void setFactureMail(Boolean factureMail) {
        this.factureMail = factureMail;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }
}
