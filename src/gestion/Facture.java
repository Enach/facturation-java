package gestion;

import java.util.Date;
import java.util.List;

/**
 * Created by enach-laptop on 24/05/2016.
 */
public class Facture {
    private int numero;
    private int client;
    private Date date;
    private Ligue ligue;
    private int prix;
    private List<Prestation> prestations;

    public Facture(int numero, int client, Date date, Ligue ligue, int price) {
        if (numero == 0) {
            this.numero = numero;
        }
        this.client = client;
        this.date = date;
        this.ligue = ligue;
        this.prix = price;
    }

    public int getNumero() {
        return numero;
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Ligue getLigue() {
        return ligue;
    }

    public void setLigue(Ligue ligue) {
        this.ligue = ligue;
    }

    public int getPrix() {
        return prix;
    }

    public List<Prestation> getPrestations() {
        return prestations;
    }

    public void addPrestation(Prestation prestation) {
        this.prestations.add(prestation);
    }
}
