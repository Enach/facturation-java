package gestion;

/**
 * Created by enach-laptop on 24/05/2016.
 */
public class Prestation {
    private int id;
    private int reference;
    private String description;
    private int prix;

    public Prestation(int reference, String description, int prix) {
        this.reference = reference;
        this.description = description;
        this.prix = prix;
    }

    public int getId() {
        return id;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
}
