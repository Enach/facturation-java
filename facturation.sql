-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 12 Juin 2016 à 10:54
-- Version du serveur :  5.5.44-0+deb8u1
-- Version de PHP :  5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `facturation`
--
CREATE DATABASE IF NOT EXISTS `facturation` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `facturation`;

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

DROP TABLE IF EXISTS `factures`;
CREATE TABLE IF NOT EXISTS `factures` (
`numero` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `facture_prestation`
--

DROP TABLE IF EXISTS `facture_prestation`;
CREATE TABLE IF NOT EXISTS `facture_prestation` (
  `facture_id` int(11) NOT NULL,
  `prestation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ligues`
--

DROP TABLE IF EXISTS `ligues`;
CREATE TABLE IF NOT EXISTS `ligues` (
`id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email_tresorier` varchar(255) NOT NULL,
  `facture_mail` tinyint(1) NOT NULL DEFAULT '1',
  `tresorier` varchar(255) NOT NULL,
  `sport` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ligue_factures`
--

DROP TABLE IF EXISTS `ligue_factures`;
CREATE TABLE IF NOT EXISTS `ligue_factures` (
  `ligue_id` int(11) NOT NULL,
  `facture_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `prestations`
--

DROP TABLE IF EXISTS `prestations`;
CREATE TABLE IF NOT EXISTS `prestations` (
`id` int(11) NOT NULL,
  `reference` int(11) NOT NULL,
  `description` text NOT NULL,
  `prix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `factures`
--
ALTER TABLE `factures`
 ADD PRIMARY KEY (`numero`);

--
-- Index pour la table `facture_prestation`
--
ALTER TABLE `facture_prestation`
 ADD KEY `facture_id` (`facture_id`,`prestation_id`), ADD KEY `facture_id_2` (`facture_id`), ADD KEY `prestation_id` (`prestation_id`);

--
-- Index pour la table `ligues`
--
ALTER TABLE `ligues`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ligue_factures`
--
ALTER TABLE `ligue_factures`
 ADD KEY `ligue_id` (`ligue_id`,`facture_id`), ADD KEY `facture_id` (`facture_id`), ADD KEY `ligue_id_2` (`ligue_id`);

--
-- Index pour la table `prestations`
--
ALTER TABLE `prestations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `reference` (`reference`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `factures`
--
ALTER TABLE `factures`
MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ligues`
--
ALTER TABLE `ligues`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `prestations`
--
ALTER TABLE `prestations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `facture_prestation`
--
ALTER TABLE `facture_prestation`
ADD CONSTRAINT `facture_prestation_ibfk_2` FOREIGN KEY (`prestation_id`) REFERENCES `prestations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `facture_prestation_ibfk_1` FOREIGN KEY (`facture_id`) REFERENCES `factures` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ligue_factures`
--
ALTER TABLE `ligue_factures`
ADD CONSTRAINT `ligue_factures_ibfk_2` FOREIGN KEY (`facture_id`) REFERENCES `factures` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `ligue_factures_ibfk_1` FOREIGN KEY (`ligue_id`) REFERENCES `ligues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;